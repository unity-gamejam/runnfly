﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChangeGravity : MonoBehaviour
{
    public float gravityScale = 1.0f;
    public float RotateSpeed = 30f;

    public static float globalGravity = -9.81f;

    Rigidbody myRigidbody;
    bool isUpsideDown = false;

    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            isUpsideDown = !isUpsideDown;
            if (isUpsideDown)
            {
                myRigidbody.useGravity = false;
                Vector3 gravity = globalGravity * gravityScale * Vector3.down;
                GetComponent<ConstantForce>().force = gravity;
            }
            else
            {
                myRigidbody.useGravity = true;
                Vector3 gravity = globalGravity * gravityScale * Vector3.up;
                GetComponent<ConstantForce>().force = gravity;
            }
            rotateCamera();
        }
    }

    private void rotateCamera()
    {
        if (isUpsideDown)
        {
           myRigidbody.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            myRigidbody.transform.localEulerAngles = new Vector3(0, 0, 0);
            //myRigidbody.transform.Rotate(new Vector3(0f, 0f, 0f) * RotateSpeed * Time.deltaTime);
        }
    }
}
