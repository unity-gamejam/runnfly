﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    private Vector3 velocity;
    private Vector3 rotation;
    private float cameraRotation = 0f;
    private float currentCameraRotation = 0f;
    [SerializeField]
    private float cameraRotationLimit = 85f;

    [SerializeField]
    private Camera camera;

    Rigidbody myRigidbody;

    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    public void Move(Vector3 velocity)
    {
        this.velocity = velocity;
    }

    public void RotateCamera(float cameraRotation)
    {
        this.cameraRotation = cameraRotation;
    }

    public void Rotate(Vector3 rotation)
    {
        this.rotation = rotation;
    }

    private void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    private void PerformMovement()
    {
        if (myRigidbody.tag.Equals("Fay"))
        {
            myRigidbody.velocity = Vector3.zero;
        }
       
        if (this.velocity != Vector3.zero)
        {
            myRigidbody.MovePosition(myRigidbody.position + this.velocity * Time.fixedDeltaTime);
        }
    }

    private void PerformRotation()
    {
        // recuperation de la rotation + clamp rotation
        myRigidbody.MoveRotation(myRigidbody.rotation * Quaternion.Euler(this.rotation));
        currentCameraRotation += -cameraRotation;
        currentCameraRotation = Mathf.Clamp(currentCameraRotation, -cameraRotationLimit, cameraRotationLimit);

        camera.transform.localEulerAngles = new Vector3(currentCameraRotation, 0f, 0f);
    }
}
