﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableGrappling : MonoBehaviour
{
    [SerializeField]
    int runnerlayer;

    [SerializeField]
    AudioSource audio;

    private void OnCollisionEnter (Collision collision) {
        if(collision.gameObject.layer == runnerlayer) {
            Debug.Log("Ton Grappin est prêt !");
            collision.gameObject.GetComponent<GrapplingHook>().enabled = true;
            //audio.Play();
        }
    }
}
