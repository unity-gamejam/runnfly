﻿using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public Camera cam;
    public RaycastHit hit;

    public LayerMask cullingMask;
    [SerializeField]
    int maxDistance = 20;

    bool isFlying;
    Vector3 loc;

    [SerializeField]
    float speed = 10f;
    public Transform hand;

    [SerializeField]
    LineRenderer lineRenderer;
    [SerializeField]
    Collider collider;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && !isFlying)
        {
            FindSpot();
        }
        if (isFlying)
        {
            Flying();
        }
        if (Input.GetKey(KeyCode.Space) && isFlying)
        {
            isFlying = false;
            lineRenderer.enabled = false;
            collider.attachedRigidbody.useGravity = true;
        }
    }

    public void FindSpot()
    {
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, maxDistance, cullingMask))
        {
            isFlying = true;
            loc = hit.point;
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(1, loc);
        }
    }

    public void Flying()
    {
        collider.attachedRigidbody.useGravity = false;
        transform.position = Vector3.Lerp(transform.position, loc, speed * Time.deltaTime / Vector3.Distance(transform.position, loc));
        lineRenderer.SetPosition(0, hand.position);

        if (Vector3.Distance(transform.position, loc) < 1f)
        {
            isFlying = false;
            lineRenderer.enabled = false;
            collider.attachedRigidbody.useGravity = true;
        }
    }
}
