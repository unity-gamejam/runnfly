﻿using UnityEngine;
using UnityEngine.Networking;

public class TakeProps : NetworkBehaviour
{
    [SerializeField]
    GameObject mainCamera;
    bool carrying;
    GameObject carriedObject;
    public float distance;
    public float smooth;
    public LayerMask mask;

    void Update()
    {
        if (carrying)
        {
            CmdCarry(carriedObject);
            CmdCheckDrop();
        }
        else
        {
            CmdPickup();
        }
    }

    [Command]
    void CmdRotateObject()
    {
        carriedObject.transform.Rotate(5, 10, 15);
    }

    [Command]
    void CmdCarry (GameObject o)
    {
        Debug.Log("carry");
        o.transform.position = Vector3.Lerp(o.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
    }

    [Command]
    void CmdPickup ()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, distance, mask))
            {
                Takable p = hit.collider.GetComponent<Takable>();
                if (p != null)
                {
                    Debug.Log("pickup");
                    carrying = true;
                    carriedObject = p.gameObject;
                    Debug.Log(p.name);
                    p.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                }
            }
        }
    }

    [Command]
    void CmdCheckDrop ()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            CmdDropObject();
        }
    }

    [Command]
    void CmdDropObject()
    {
        carrying = false;
        carriedObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        carriedObject = null;
    }
}
