﻿using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(PlayerMovement))]
public class RunnerController : MonoBehaviour
{
    [SerializeField]
    float walkSpeed;
    [SerializeField]
    float runSpeed;
    float run = 1;
    [SerializeField]
    float jumpForce;
    [SerializeField]
    float lookSensitivity = 4f;

    PlayerMovement movement;
    Transform myTransform;
    Rigidbody myRigidbody;

    float xMov;
    float yMov;
    float zMov;
    float xRot;
    float yRot;

    Animator animator;
    public bool isWalking = false;
    public bool isRunning = false;
    public bool isGrounded = true; //TODO

    private void Start()
    {
        movement = GetComponent<PlayerMovement>();
        myTransform = transform;
        myRigidbody = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        HideMouse();
        ReadInput();
        CheckAction();
        UpdateAnimation();
    }

    private void ReadInput()
    {
        xMov = Input.GetAxis("Horizontal");
        yMov = Input.GetAxis("Jump");
        zMov = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift))
        {
            run = runSpeed;
        }
        else
        {
            run = 1f;
        }

        Vector3 movHorizontal = myTransform.right * xMov;
        Vector3 movVertical = myTransform.forward * zMov;

        Vector3 velocity = (movHorizontal + movVertical).normalized * walkSpeed * run;
        velocity.y = yMov * jumpForce;

        movement.Move(velocity);


        yRot = Input.GetAxis("Mouse X");
        Vector3 rotation = new Vector3(0f, yRot, 0f) * lookSensitivity;
        movement.Rotate(rotation);

        xRot = Input.GetAxis("Mouse Y");
        float cameraRotation = xRot * lookSensitivity;
        movement.RotateCamera(cameraRotation);
    }

    void CheckAction()
    {
        isWalking = zMov > 0.001 && !isRunning ? true : false;
        isRunning = run > 1 && zMov > 0.001 ? true : false;
    }

    void UpdateAnimation()
    {
        animator.SetBool("isWalking", isWalking);
        animator.SetBool("isRunning", isRunning);
    }

    void HideMouse () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        } else if (Input.GetMouseButtonUp(0)) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
