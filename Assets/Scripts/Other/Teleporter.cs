﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Teleporter : MonoBehaviour
{
    [SerializeField]
    int runnerLayer;

    [SerializeField]
    int fayLayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == runnerLayer)
        {
            GameObject.FindGameObjectWithTag("Runner").transform.position = GameObject.FindGameObjectWithTag("TeleportRunner").transform.position;
            GameObject.FindGameObjectWithTag("Spawner").transform.position = GameObject.FindGameObjectWithTag("TeleportRunner").transform.position;
        }
        else if (other.gameObject.layer == fayLayer)
        {
            GameObject.FindGameObjectWithTag("Fay").transform.position = GameObject.FindGameObjectWithTag("TeleportFay").transform.position;
        }
    }
}
