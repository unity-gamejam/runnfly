﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPowder : MonoBehaviour
{
    [SerializeField]
    int energyRegained;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == transform.gameObject.layer)
        {
            GameObject fay = other.gameObject;
            EnergyController energyController = fay.GetComponent<EnergyController>();
            energyController.CmdRegenEnergy(energyRegained);

            Destroy(transform.gameObject);
        }
    }
}
