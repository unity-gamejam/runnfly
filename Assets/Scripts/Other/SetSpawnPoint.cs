﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSpawnPoint : MonoBehaviour
{
    [SerializeField]
    int runnerLayer;

    private void OnTriggerEnter (Collider other) {
        if(other.gameObject.layer == runnerLayer) {
            Debug.Log(GameObject.FindGameObjectWithTag("Respawn").transform.position);
            GameObject.FindGameObjectWithTag("Respawn").transform.position = transform.position;
            Debug.Log(GameObject.FindGameObjectWithTag("Respawn").transform.position);
        }
    }
}
