﻿using UnityEngine;

public class ShadowBox : MonoBehaviour
{
    private bool onetime;
    public Color boxColor = Color.green;

    void OnTriggerEnter (Collider target) {
        if (target.GetComponent<Detectable>())
            target.GetComponent<Detectable>().detectable = false;
        else if (target.GetComponent<AiEnemy>()) 
            target.GetComponent<AiEnemy>().generalParameters.mainTarget.gameObject.GetComponent<Detectable>().detectable = true;   
    }

    void OnTriggerStay (Collider target) {
        if (target.GetComponent<Detectable>() && !onetime) {
            onetime = true;
            target.GetComponent<Detectable>().detectable = false;
        }

    }

    void OnTriggerExit (Collider target) {
        if (target.GetComponent<Detectable>()) {
            target.GetComponent<Detectable>().detectable = true;
            onetime = false;
        }
    }
}
