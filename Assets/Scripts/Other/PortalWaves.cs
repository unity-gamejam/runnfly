﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class PortalWaves : MonoBehaviour
{
    [SerializeField]
    Renderer renderer;
    public Vector2 normal1dir;
    public float normal1speed;
    public Vector2 normal2dir;
    public float normal2speed;
    Material material;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        material = renderer.material;
    }

    // Update is called once per frame
    void Update()
    {
        float offset1 = Time.time * normal1speed;
        material.SetTextureOffset("_MainTex", normal1dir * offset1);
        material.SetTextureOffset("_DetailAlbedoMap", normal2dir * Time.time * normal2speed);
    }
}
