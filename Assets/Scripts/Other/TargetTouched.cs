﻿using UnityEngine;
using UnityEngine.Networking;

public class TargetTouched : MonoBehaviour
{
    [SerializeField]
    GameObject objectToBeDestroyed;

    [SerializeField]
    int coucheProps = 8;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == coucheProps)
        {
            NetworkServer.Destroy(objectToBeDestroyed);
        }
    }
}
