﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfTheMap : MonoBehaviour
{
    [SerializeField]
    int playerlayer;

    private void OnCollisionEnter (Collision collision) {
        if(collision.gameObject.layer == playerlayer) {
            collision.gameObject.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
        }
    }
}
