﻿using UnityEngine;

public class RunnerAudio : MonoBehaviour
{
    [SerializeField]
    RunnerController runner;
    [SerializeField]
    AudioSource walkingAudio;
    [SerializeField]
    AudioSource runningAudio;

    private void Start () {
        runner = GetComponent<RunnerController>();
    }

    private void Update () {
        if (runner.isGrounded && runner.isWalking && !walkingAudio.isPlaying) {
            walkingAudio.Play();
        } else if (runner.isGrounded && runner.isRunning && !runningAudio.isPlaying) {
            walkingAudio.Stop();
            runningAudio.Play();
        } else if (!runner.isWalking) {
            walkingAudio.Stop();
        } else if (!runner.isRunning) {
            runningAudio.Stop();
        }
    }
}
