﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    [SerializeField]
    int runnerLayer;

    private void OnTriggerEnter (Collider other) {
        if(other.gameObject.layer == runnerLayer) {
            Debug.Log("Other: " + other.gameObject.name);
            other.gameObject.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
        }
    }
}
