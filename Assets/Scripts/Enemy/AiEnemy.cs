﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[Serializable]
public class General {
    public Transform mainTarget;
    public string mainTargetTag;

    public bool disappearOnDeath;
    public float disappearDelay = 2.0f;

    public bool aiPhysics;
    public float aiMass = 5f;
}

[Serializable]
public class NavigationMovement {
    public float goIdleRadius = 35.0f;
    public float acceleration = 8f;
    public float arrivalDistance = 1.5f;
    public float chasingSpeed = 5.0f;
    public float hearingDistance = 20.0f;
    public float lookAtSpeed = 6.0f;
    public float patrolSpeed = 2.5f;
    public float rotationSpeed = 140.0f;
    public Vector3 viewSphereCenter = new Vector3(0, 0, 8.5f);
    public float viewSphereRadius = 10.0f;
    public float wanderingRadius = 10.0f;
    public float maxIdleInterval = 7.0f;
    public float minIdleInterval = 2.0f;
    public float lineOfSightHeight = 0.5f;
}

[Serializable]
public class LayerGroup {
    public LayerMask mainTarget;
    public LayerMask viewObstruction;
}

public class AiEnemy : MonoBehaviour {
    private enum PatrolMode {
        In_Place,
        Dynamic_Wandering,
        Waypoints
    }

    [Header("General Parameters")]
    [SerializeField]
    public General generalParameters;

    private State state = State.IDLE;
    private State lastState = State.IDLE;
    [SerializeField]
    private MeshRenderer stateIndicator;

    [Header("Layers")]
    [SerializeField]
    private LayerGroup layers;

    private bool rotateUpdate;

    /* Detection Runner */
    private GameObject triggerChil;
    //[HideInInspector] public Collider other;
    private bool isExited = true;

    [Header("Patrol and navigations")]
    [SerializeField]
    private NavMeshAgent nav;

    [SerializeField]
    private NavigationMovement navigationMovement;

    [SerializeField]
    private PatrolMode patrolMode;
    private bool wanderingSwitch = true;
    private bool waypointSwitch = true;
    private NavMeshPath path;
    private Vector3 rPosition;
    [SerializeField]
    private GameObject patrolPath;
    private Transform[] wpTransforms;
    private int waypointCount;

    //Hide and Show waypoints
    private bool showHide = true;

    [SerializeField]
    public bool randomizePath;

    [SerializeField]
    private Color linesColor = Color.green;

    private void Start () {
        if (!generalParameters.mainTarget) {
            if (GameObject.FindGameObjectWithTag(generalParameters.mainTargetTag)) {
                generalParameters.mainTarget = GameObject.FindGameObjectWithTag(generalParameters.mainTargetTag).transform;
                if (!generalParameters.mainTarget.GetComponent<Detectable>())
                    generalParameters.mainTarget.gameObject.AddComponent<Detectable>();
            }
        } else {
            if (!generalParameters.mainTarget.GetComponent<Detectable>())
                generalParameters.mainTarget.gameObject.AddComponent<Detectable>();
        }

        triggerChil = new GameObject();
        triggerChil.AddComponent<Rigidbody>();
        triggerChil.GetComponent<Rigidbody>().isKinematic = true;
        triggerChil.transform.parent = transform;
        triggerChil.transform.localPosition = Vector3.zero;
        triggerChil.AddComponent<SphereCollider>();
        triggerChil.layer = LayerMask.NameToLayer("Ignore Raycast");
        triggerChil.AddComponent<IdleSphere>();
        triggerChil.name = "TriggerSphere";
        triggerChil.GetComponent<SphereCollider>().radius = navigationMovement.goIdleRadius;
        triggerChil.GetComponent<SphereCollider>().isTrigger = true;

        if (!GetComponent<NavMeshAgent>()) gameObject.AddComponent<NavMeshAgent>();

        if (generalParameters.aiPhysics) {
            if (!GetComponent<Rigidbody>()) gameObject.AddComponent<Rigidbody>();
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            GetComponent<Rigidbody>().drag = 2;
            GetComponent<Rigidbody>().mass = generalParameters.aiMass;
        }


        nav = gameObject.GetComponent<NavMeshAgent>();
        nav.speed = navigationMovement.patrolSpeed;
        nav.angularSpeed = navigationMovement.rotationSpeed;
        nav.stoppingDistance = navigationMovement.arrivalDistance;
        nav.acceleration = navigationMovement.acceleration;

        if (layers.mainTarget == 0)
            Debug.LogWarning("Il ne faut assigner que 1 layer: " +
                             generalParameters.mainTarget.gameObject.name + ", in AI: " +
                             gameObject.name);

        /*if (layers.viewObstruction.value == 0)
            Debug.LogWarning("Il faut assigner une couche d'obstruction à la vue: " + gameObject.name);*/

        if (patrolMode == PatrolMode.Waypoints) {
            Transform wp = patrolPath.transform;
            int wpcount = wp.childCount;
            wpTransforms = new Transform[wpcount];

            for (int i = 0; i < wpcount; i++) {
                wpTransforms[i] = wp.GetChild(i);
            }
        }

        UnawareMode();
    }


    private void Update () {
        if (!generalParameters.mainTarget) {
            if (GameObject.FindGameObjectWithTag(generalParameters.mainTargetTag)) {
                generalParameters.mainTarget = GameObject.FindGameObjectWithTag(generalParameters.mainTargetTag).transform;
                if (!generalParameters.mainTarget.GetComponent<Detectable>())
                    generalParameters.mainTarget.gameObject.AddComponent<Detectable>();
            }
        }

        if (rotateUpdate) {
            Vector3 targetDir = generalParameters.mainTarget.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(targetDir);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation,
                Time.deltaTime * navigationMovement.lookAtSpeed);

            Vector3 forward = transform.forward;
            float angle = Vector3.Angle(targetDir, forward);
            if (angle < 20.0f) {
                rotateUpdate = false;
            }
        }

        if ((!waypointSwitch || !wanderingSwitch) && nav.remainingDistance <= navigationMovement.arrivalDistance && !state.isIdle())
            StartCoroutine("IdleInWandering");

        if (state.isDeath() && generalParameters.disappearOnDeath) {
            Destroy(gameObject, generalParameters.disappearDelay);
        }

        if (nav && (nav.pathStatus == NavMeshPathStatus.PathInvalid || nav.pathStatus == NavMeshPathStatus.PathPartial)) {
            wanderingSwitch = true;
        }

        if (state.isPursue()) {
            nav.SetDestination(generalParameters.mainTarget.position);
        } else if (waypointSwitch && wanderingSwitch) {
            UnawareMode();
        }
    }

    public void TriggerExit () {
        isExited = true;
        GoIdle();
        nav.isStopped = true;
        wanderingSwitch = true;
    }

    public void TriggerStay () {
        isExited = false;
        Collider[] colinfo = Physics.OverlapSphere(transform.TransformPoint(navigationMovement.viewSphereCenter), navigationMovement.viewSphereRadius, layers.mainTarget);

        foreach (Collider colhit in colinfo) {
            if (colinfo.Length == 0) {
                UnawareMode();
                return;
            }
            
            if (colhit.gameObject.tag == generalParameters.mainTargetTag) {
                RaycastHit hitinfo;
                if (!Physics.Linecast(transform.position + new Vector3(0, navigationMovement.lineOfSightHeight, 0), generalParameters.mainTarget.position, out hitinfo,
                        layers.viewObstruction)) {
                    if (generalParameters.mainTarget.GetComponent<Detectable>().detectable) {
                        lastState = state;
                        state = State.PURSUE;
                        state.setColorIndicator(stateIndicator);
                        waypointSwitch = true;
                        wanderingSwitch = true;
                        waypointCount = 0;
                        StopCoroutine("IdleInWandering");

                        if (!nav.isOnOffMeshLink) nav.speed = navigationMovement.chasingSpeed;

                        nav.SetDestination(generalParameters.mainTarget.position);
                    } else {
                        UnawareMode();
                    }
                } else {
                    UnawareMode();
                }
            }
        }
    }

    private void UnawareMode () {
        switch (patrolMode) {
            case PatrolMode.In_Place:
                GoIdle();
                break;
            case PatrolMode.Dynamic_Wandering:
                if (wanderingSwitch) {
                    nav.speed = navigationMovement.patrolSpeed;
                    Wandering();
                }
                break;
            case PatrolMode.Waypoints:
                if (waypointSwitch) {
                    nav.speed = navigationMovement.patrolSpeed;
                    PatrolWayPoint();
                }
                break;
        }
    }

    private void Wandering () {
        wanderingSwitch = false;
        rPosition = new Vector3(
            transform.position.x + (Random.Range(-1.0f, 1.0f) * navigationMovement.wanderingRadius),
            transform.position.y,
            transform.position.z + (Random.Range(-1.0f, 1.0f) * navigationMovement.wanderingRadius));


        path = new NavMeshPath();

        bool canit = NavMesh.CalculatePath(transform.position, rPosition, 1, path);
        if (canit) {
            state = State.WANDERING;
            state.setColorIndicator(stateIndicator);
            nav.SetDestination(rPosition);
            nav.isStopped = false;
        } else wanderingSwitch = true;
    }

    private void OnDrawGizmos () {
        if (!wanderingSwitch) {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(rPosition, 0.5f);
        }

        linesColor = new Color(linesColor.r, linesColor.g, linesColor.b, 255f);
        Gizmos.color = linesColor;
        if (showHide && patrolMode == PatrolMode.Waypoints) {
            int WpC = patrolPath.transform.childCount;
            for (int t = 0; t < WpC; t++) {
                Gizmos.DrawSphere(patrolPath.transform.GetChild(t).gameObject.transform.position, 0.35f);
                if (t < (WpC - 1)) {
                    Gizmos.DrawLine(
                        patrolPath.transform.GetChild(t).gameObject.transform.position,
                        patrolPath.transform.GetChild(t + 1).gameObject.transform.position);
                } else if (t == (WpC - 1)) {
                    Gizmos.DrawLine(
                        patrolPath.transform.GetChild(t).gameObject.transform.position,
                        patrolPath.transform.GetChild(0).gameObject.transform.position);
                }
            }
        }
    }

    private void OnDrawGizmosSelected () {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.TransformPoint(navigationMovement.viewSphereCenter),
            navigationMovement.viewSphereRadius);

        Gizmos.DrawRay(transform.position + new Vector3(0, navigationMovement.lineOfSightHeight, 0), transform.forward);

        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, navigationMovement.goIdleRadius);

        if (patrolMode == PatrolMode.Dynamic_Wandering) {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, navigationMovement.wanderingRadius);
        }
    }

    private IEnumerator IdleInWandering () {
        lastState = state;
        GoIdle();
        float waitingtime = Random.Range(navigationMovement.minIdleInterval, navigationMovement.maxIdleInterval);

        yield return new WaitForSeconds(waitingtime);

        if (lastState.isPatrol()) {
            if (waypointCount == (wpTransforms.Length - 1)) {
                if (!randomizePath) waypointCount = 0;
                else waypointCount = Random.Range(0, wpTransforms.Length);
            } else {
                if (!randomizePath) waypointCount++;
                else waypointCount = Random.Range(0, wpTransforms.Length);
            }
        }
        waypointSwitch = true;
        wanderingSwitch = true;

        state = lastState;
        state.setColorIndicator(stateIndicator);
    }

    private void GoIdle () {
        if (state.isIdle()) return;
        state = State.IDLE;
        state.setColorIndicator(stateIndicator);
    }

    private void ShowHide () {
        if (patrolMode == PatrolMode.Waypoints) {
            showHide = !showHide;
        }
    }

    private void PatrolWayPoint () {
        waypointSwitch = false;
        state = State.PATROL;
        state.setColorIndicator(stateIndicator);
        nav.SetDestination(wpTransforms[waypointCount].position);
        nav.isStopped = false;
    }
}
