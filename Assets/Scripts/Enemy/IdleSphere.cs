﻿using UnityEngine;

public class IdleSphere : MonoBehaviour {
    private AiEnemy AiEnemy;

    void Start () {
        if (transform.parent.gameObject.GetComponent<AiEnemy>())
            AiEnemy = transform.parent.gameObject.GetComponent<AiEnemy>();
    }

    private void OnTriggerStay (Collider other) {
        if (other.gameObject.layer == 10)
            this.AiEnemy.TriggerStay();
    }

    private void OnTriggerExit (Collider other) {
        if (other.gameObject.layer == 10)
            this.AiEnemy.TriggerExit();
    }
}
