﻿using UnityEngine;

public enum State {
    IDLE,
    WANDERING,
    PATROL,
    ALERT,
    PURSUE,
    MELEE_ATTACK,
    DEATH,
}

public static class StateExtends {

    public static bool isIdle (this State state) {
        if (state.Equals(State.IDLE)) {
            return true;
        }

        return false;
    }

    public static bool isWandering (this State state) {
        if (state.Equals(State.WANDERING)) {
            return true;
        }

        return false;
    }

    public static bool isPatrol (this State state) {
        if (state.Equals(State.PATROL)) {
            return true;
        }

        return false;
    }

    public static bool isAlert (this State state) {
        if (state.Equals(State.ALERT)) {
            return true;
        }

        return false;
    }

    public static bool isPursue (this State state) {
        if (state.Equals(State.PURSUE)) {
            return true;
        }

        return false;
    }

    public static bool isMeleeAttack (this State state) {
        if (state.Equals(State.MELEE_ATTACK)) {
            return true;
        }

        return false;
    }

    public static bool isDeath (this State state) {
        if (state.Equals(State.DEATH)) {
            return true;
        }

        return false;
    }

    public static void setColorIndicator (this State state, MeshRenderer indicator) {
        switch (state) {
            case State.IDLE:
                indicator.material.color = Color.green;
                break;
            case State.WANDERING:
                indicator.material.color = Color.cyan;
                break;
            case State.PATROL:
                indicator.material.color = Color.blue;
                break;
            case State.PURSUE:
                indicator.material.color = Color.red;
                break;
            case State.MELEE_ATTACK:
                break;
            case State.DEATH:
                break;
        }
    }
}
