﻿using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class RunNFlyNetworkManager : NetworkManager
{
    [SerializeField]
    GameObject fayPrefab;
    [SerializeField]
    GameObject runnerPrefab;

    [SerializeField]
    GameObject spawnFay;
    [SerializeField]
    GameObject spawnRunner;

    short playerControllerHighestId = 1;

    public override void OnStartClient (NetworkClient client) {
        base.OnStartClient(client);

        ClientScene.RegisterPrefab(fayPrefab);
        ClientScene.RegisterPrefab(runnerPrefab);
    }
    
    public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId) {
        if (playerControllerHighestId == 2) {
            GameObject newPlayer = GameObject.Instantiate(runnerPrefab);
            newPlayer.transform.position = spawnRunner.transform.position;
            NetworkServer.Spawn(newPlayer);

            NetworkServer.AddPlayerForConnection(conn, newPlayer, playerControllerId);
            
        } else {
            GameObject newPlayer = GameObject.Instantiate(fayPrefab);
            newPlayer.transform.position = spawnFay.transform.position;
            NetworkServer.Spawn(newPlayer);

            NetworkServer.AddPlayerForConnection(conn, newPlayer, playerControllerId);
            playerControllerHighestId++;
        }   
    }

    public override void OnServerRemovePlayer (NetworkConnection conn, PlayerController player) {
        base.OnServerRemovePlayer(conn, player);
        playerControllerHighestId--;
    }

    public override void OnStopHost () {
        base.OnStopHost();
        playerControllerHighestId = 0;
    }
}
