﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(PlayerMovement))]
public class FayController : MonoBehaviour
{
    [SerializeField]
    float walkSpeed;
    [SerializeField]
    float runSpeed;
    float run = 1;
    [SerializeField]
    float jumpForce;
    [SerializeField]
    float downForce = -1f;
    [SerializeField]
    float lookSensitivity = 4f;

    PlayerMovement movement;
    Transform myTransform;
    Rigidbody myRigidbody;
    GameObject runner;

    float xMov;
    float yMov;
    float zMov;
    float xRot;
    float yRot;

    private void Start()
    {
        movement = GetComponent<PlayerMovement>();
        myTransform = transform;
        myRigidbody = GetComponent<Rigidbody>();
        runner = GameObject.FindGameObjectWithTag("Runner");
    }

    
    private void Update()
    {
        if (runner == null) {
            runner = GameObject.FindGameObjectWithTag("Runner");
            if (runner == null) return;
        }

        HideMouse();
        ReadInput();
    }

    private void ReadInput()
    {
        xMov = Input.GetAxis("Horizontal");
        yMov = Input.GetAxis("Jump");
        zMov = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift))
        {
            run = runSpeed;
        }
        else
        {
            run = 1f;
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            yMov = downForce;
        }

        Vector3 movHorizontal = myTransform.right * xMov;
        Vector3 movVertical = myTransform.forward * zMov;

        Vector3 velocity = (movHorizontal + movVertical).normalized * walkSpeed * run;
        velocity.y = yMov * jumpForce;

        movement.Move(velocity);


        yRot = Input.GetAxis("Mouse X");
        Vector3 rotation = new Vector3(0f, yRot, 0f) * lookSensitivity;
        movement.Rotate(rotation);

        xRot = Input.GetAxis("Mouse Y");
        float cameraRotation = xRot * lookSensitivity;
        movement.RotateCamera(cameraRotation);
    }

    void HideMouse () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        } else if (Input.GetMouseButtonUp(0)) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
