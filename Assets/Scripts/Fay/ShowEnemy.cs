﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEnemy : MonoBehaviour {
    [SerializeField]
    LayerMask enemyLayers;
    [SerializeField]
    int showMask;
    [SerializeField]
    int enemyLayer;

    [SerializeField]
    int showTime;

    IList<GameObject> listEnemy;

    private void Start () {
        listEnemy = new List<GameObject>();
    }

    private void OnTriggerEnter (Collider other) {
        GameObject enemy;
        if (enemyLayers == (enemyLayers | 1 << other.gameObject.layer)) {
            enemy = other.gameObject;
            if (!listEnemy.Contains(enemy)) {
                listEnemy.Add(enemy);
                StartCoroutine(ShowEnemyTime(enemy));
            }
        }
        else {
            transform.GetComponent<Collider>().isTrigger = false;
        }
    }

    IEnumerator ShowEnemyTime (GameObject enemy) {
        enemy.layer = showMask;
        enemy.transform.GetChild(0).gameObject.layer = showMask;
        yield return new WaitForSeconds(showTime);
        enemy.layer = enemyLayer;
        enemy.transform.GetChild(0).gameObject.layer = enemyLayer;
        listEnemy.Remove(enemy);
    }
}