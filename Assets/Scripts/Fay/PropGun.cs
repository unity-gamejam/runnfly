﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

[System.Obsolete]
public class PropGun : NetworkBehaviour
{
    [SerializeField]
    GameObject smallProp;
    [SerializeField]
    GameObject bigProp;
    [SerializeField]
    float lifeTime;

    [SerializeField]
    Camera camera;
    [SerializeField]
    GameObject propSpawn;

    [SerializeField]
    float propSpeed;

    [SerializeField]
    float smallPropForce;
    [SerializeField]
    float bigPropForce;

    [SerializeField]
    float fireIntervalSmall;
    bool canShoot = true;

    [SerializeField]
    int smallPropCost;
    [SerializeField]
    int bigPropCost;

    [SerializeField]
    AudioSource littleBopAudio;
    [SerializeField]
    AudioSource bigBopAudio;

    GameObject runner;
    GameObject fay;
    EnergyController energyController;

    private void Start () {
        runner = GameObject.FindGameObjectWithTag("Runner");
        fay = GameObject.FindGameObjectWithTag("Fay");
        energyController = fay.GetComponent<EnergyController>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && canShoot)
        {
            CmdShootSmallProp();
            StartCoroutine(ShootInterval());
        }
        else if (Input.GetButtonDown("Fire2") && canShoot)
        {
            if (runner == null) {
                runner = GameObject.FindGameObjectWithTag("Runner");
                if (runner == null) return;
            }

            CmdShootBigProp();
            StartCoroutine(ShootInterval());
        }
    }

    [Command]
    private void CmdShootSmallProp()
    {
        GameObject props = Pooling.Acquire(smallProp);
        props.transform.position = propSpawn.transform.position;
        props.GetComponent<Rigidbody>().AddForce(transform.forward * smallPropForce);
        props.GetComponent<Rigidbody>().velocity = transform.forward * propSpeed;
        props.GetComponent<Rigidbody>().useGravity = true;

        NetworkServer.Spawn(props);

        energyController.CmdUseEnergy(smallPropCost);
        littleBopAudio.Play();
    }

    [Command]
    private void CmdShootBigProp () {
        GameObject props = Pooling.Acquire(bigProp);
        props.transform.position = propSpawn.transform.position;
        props.GetComponent<Rigidbody>().AddForce(transform.forward * bigPropForce);
        props.GetComponent<Rigidbody>().velocity = transform.forward * propSpeed;
        props.GetComponent<Rigidbody>().useGravity = true;

        NetworkServer.Spawn(props);

        energyController.CmdUseEnergy(bigPropCost);
        bigBopAudio.Play();
    }

    IEnumerator ShootInterval()
    {
        canShoot = false;
        yield return new WaitForSeconds(fireIntervalSmall);
        canShoot = true;
    }
}
