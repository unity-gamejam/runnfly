﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindScript : MonoBehaviour
{
    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    private Dictionary<string, KeyCode> keysBackup;

    public Text up, left, down, right, jump, fire1, fire2, props, run;

    private GameObject currentKey;

    void Start()
    {
        keys.Add("Up", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up", "Z")));
        keys.Add("Down", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down", "S")));
        keys.Add("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left", "Q")));
        keys.Add("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "D")));
        keys.Add("Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump", "Space")));
        keys.Add("Fire1", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Fire1", "Mouse0")));
        keys.Add("Fire2", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Fire2", "Mouse1")));
        keys.Add("Props", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Props", "E")));
        keys.Add("Run", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Run", "LeftShift")));
        keysBackup = new Dictionary<string, KeyCode>(keys);
    }

    void Update()
    {
        up.text = keys["Up"].ToString();
        down.text = keys["Down"].ToString();
        left.text = keys["Left"].ToString();
        right.text = keys["Right"].ToString();
        jump.text = keys["Jump"].ToString();
        fire1.text = keys["Fire1"].ToString();
        fire2.text = keys["Fire2"].ToString();
        props.text = keys["Props"].ToString();
        run.text = keys["Run"].ToString();
    }

    void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                keys[currentKey.transform.GetChild(0).name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
            }
            else if (e.isMouse)
            {
                KeyCode keyCode;
                switch (e.button)
                {
                    case 0:
                        keyCode = KeyCode.Mouse0;
                        break;
                    case 1:
                        keyCode = KeyCode.Mouse1;
                        break;
                    case 2:
                        keyCode = KeyCode.Mouse2;
                        break;
                    case 3:
                        keyCode = KeyCode.Mouse3;
                        break;
                    case 4:
                        keyCode = KeyCode.Mouse4;
                        break;
                    case 5:
                        keyCode = KeyCode.Mouse5;
                        break;
                    case 6:
                        keyCode = KeyCode.Mouse6;
                        break;
                    default:
                        keyCode = KeyCode.None;
                        break;
                }
                keys[currentKey.transform.GetChild(0).name] = keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = keyCode.ToString();
                currentKey = null;
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        currentKey = clicked;
    }

    public void SaveKeys()
    {
        foreach (var key in keys)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }
        PlayerPrefs.Save();
        keysBackup = new Dictionary<string, KeyCode>(keys);
    }

    public void CancelChange()
    {
        keys = new Dictionary<string, KeyCode>(keysBackup);
        Update();
    }

    public void ResetKeys()
    {
        keys["Up"] = KeyCode.Z;
        keys["Down"] = KeyCode.S;
        keys["Left"] = KeyCode.Q;
        keys["Right"] = KeyCode.D;
        keys["Jump"] = KeyCode.Space;
        keys["Fire1"] = KeyCode.Mouse0;
        keys["Fire2"] = KeyCode.Mouse1;
        keys["Props"] = KeyCode.E;
        keys["Run"] = KeyCode.LeftShift;
        Update();
    }
}
