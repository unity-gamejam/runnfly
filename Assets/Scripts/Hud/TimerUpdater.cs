﻿using UnityEngine;
using UnityEngine.UI;

public class TimerUpdater : MonoBehaviour
{
    public float currentTimer;
    public float initialTimer = 300;

    [SerializeField]
    public Text timerText;

    void Start()
    {
        currentTimer = initialTimer;
    }

    void Update()
    {
        timerText.text = " "+currentTimer;
    }
}