﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HUDController : MonoBehaviour
{
    [SerializeField]
    GameObject hud;

    void Start()
    {
        Instantiate(hud);
    }
}
