﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class TimerController : NetworkBehaviour
{
    [SyncVar]
    public float timeLeft;
    public float initialTime = 300;

    public override void OnStartServer()
    {
        timeLeft = initialTime;
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
    }

    [Command]
    public void CmdResetTimer()
    {
        timeLeft = initialTime;
    }

    [Command]
    public void CmdUseTimer(int timerUsed)
    {
        timeLeft -= timerUsed;
    }

    [Command]
    public void CmdRegenTimer(int timerRegained)
    {
        timeLeft += timerRegained;
    }

    [ClientCallback]
    void Update()
    {
        GameObject playerHud = GameObject.FindGameObjectWithTag("PlayerHUD");
        TimerUpdater timerUpdater = playerHud.GetComponent<TimerUpdater>();
        timerUpdater.currentTimer = timeLeft;
    }

    IEnumerator LoseTime()
    {
        while (true)
        {
            if (timeLeft <= 0)
            {
                GameObject respawn = GameObject.Find("Respawn");
                CmdResetTimer();

                GameObject fay = GameObject.FindGameObjectWithTag("Fay");
                EnergyController energyController = fay.GetComponent<EnergyController>();
                energyController.fairyEnergy = energyController.initialFairyEnergy;

                GameObject player = GameObject.FindGameObjectWithTag("Fay");
                respawn.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint Fay").transform.position;
                player.transform.position = respawn.transform.position;

                player = GameObject.FindGameObjectWithTag("Runner");
                respawn.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint Runner").transform.position;
                player.transform.position = respawn.transform.position;
            }
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
