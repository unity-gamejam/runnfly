﻿using UnityEngine;
using UnityEngine.Networking;

public class EnergyController : NetworkBehaviour
{
    [SyncVar]
    public float fairyEnergy;
    public float initialFairyEnergy = 100;

    public override void OnStartServer()
    {
        fairyEnergy = initialFairyEnergy;
    }

    [Command]
    public void CmdUseEnergy(int energyUsed)
    {
        fairyEnergy -= energyUsed;
    }

    [Command]
    public void CmdRegenEnergy(int energyRegained)
    {
        fairyEnergy += energyRegained;
    }

    [ClientCallback]
    void Update()
    {
        GameObject playerHud = GameObject.FindGameObjectWithTag("PlayerHUD");
        EnergyUpdater energyUpdater = playerHud.GetComponent<EnergyUpdater>();
        if (fairyEnergy <= 0)
        {
            GameObject respawn = GameObject.Find("Respawn");

            GameObject fay = GameObject.FindGameObjectWithTag("Fay");
            TimerController timerController = fay.GetComponent<TimerController>();
            timerController.timeLeft = timerController.initialTime;

            fairyEnergy = energyUpdater.initialFairyEnergy;

            GameObject player = GameObject.FindGameObjectWithTag("Fay");
            respawn.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint Fay").transform.position;
            player.transform.position = respawn.transform.position;

            player = GameObject.FindGameObjectWithTag("Runner");
            respawn.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint Runner").transform.position;
            player.transform.position = respawn.transform.position;
        }
        energyUpdater.currentFairyEnergy = fairyEnergy < energyUpdater.maxFairyEnergy ? fairyEnergy : energyUpdater.maxFairyEnergy;
    }
}
