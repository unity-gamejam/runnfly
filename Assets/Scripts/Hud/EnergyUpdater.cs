﻿using UnityEngine;
using UnityEngine.UI;

public class EnergyUpdater : MonoBehaviour
{
    public float maxFairyEnergy;
    public float currentFairyEnergy;
    public float initialFairyEnergy = 100;

    [SerializeField]
    public Text fairyEnergyText;
    [SerializeField]
    public Image fairyEnergyImage;

    void Start()
    {
        currentFairyEnergy = initialFairyEnergy;
        maxFairyEnergy = initialFairyEnergy;
    }

    void Update()
    {
        fairyEnergyImage.fillAmount = currentFairyEnergy / maxFairyEnergy;
        fairyEnergyText.text = currentFairyEnergy / maxFairyEnergy * 100 + "%";
    }
}