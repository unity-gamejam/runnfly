﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool
{
    List<Poolable> poll = new List<Poolable>();
    Poolable prefab;

    public ObjectPool(Poolable prefab)
    {
        this.prefab = prefab;
    }

    public Poolable Acquire()
    {
        Poolable obj;
        if (poll.Count > 0)
        {
            obj = poll[poll.Count - 1];
            poll.RemoveAt(poll.Count - 1);
        }
        else
        {
            obj = Object.Instantiate(prefab);
        }

        obj.IsPooled = false;
        return obj;
    }

    public bool Release(Poolable obj)
    {
        if (!obj.IsPooled)
        {
            obj.IsPooled = true;
            poll.Add(obj);
            return true;
        }
        return false;
    }

    public void Remove(Poolable obj)
    {
        if (obj.IsPooled)
        {
            obj.IsPooled = false;
            poll.Remove(obj);
        }
    }
}