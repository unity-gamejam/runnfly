﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour
{

    ObjectPool pool;
    bool isPooled;
    bool isBeingDestroyed;

    public bool IsPooled
    {
        get { return isPooled; }
        set { isPooled = value; if (!isBeingDestroyed) { gameObject.SetActive(!isPooled); } }
    }

    public Poolable Acquire()
    {
        if (ReferenceEquals(pool, null))
        {
            pool = new ObjectPool(this);
        }
        Poolable obj = pool.Acquire();
        obj.pool = this.pool;
        return obj;

    }

    public void RemoveFromPool()
    {
        if (pool != null)
        {
            pool.Remove(this);
            pool = null;
        }
    }

    private void OnDestroy()
    {
        isBeingDestroyed = true;
        RemoveFromPool();
    }

    public void ReleaseImmediate()
    {
        if (!ReferenceEquals(pool, null))
        {
            pool.Release(this);
        }
    }

    public bool Release()
    {
        if (ReferenceEquals(pool, null))
        {
            return false;
        }
        return pool.Release(this);
    }
}