﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pooling
{
    static System.Func<GameObject, GameObject> fallbackAcquireDelegate = DefaultAcquireFallback;
    static System.Func<GameObject, bool> fallbackReleaseDelegate = DefaultReleaseFallback;

    public static GameObject Acquire(GameObject obj, System.Func<GameObject, GameObject> fallback)
    {
        var poolable = obj.GetComponent<Poolable>();
        if (ReferenceEquals(poolable, null))
            return fallback(obj);
        return poolable.Acquire().gameObject;
    }

    public static GameObject Acquire(GameObject obj)
    {
        return Acquire(obj, fallbackAcquireDelegate);
    }

    static GameObject DefaultAcquireFallback(GameObject obj)
    {
        return Object.Instantiate(obj);
    }

    public static bool Release(GameObject obj, System.Func<GameObject, bool> fallback)
    {
        var poolable = obj.GetComponent<Poolable>();
        if (ReferenceEquals(poolable, null))
            return fallback(obj);
        if (poolable.Release()) return true;
        else return fallback(obj);

    }

    public static bool Release(GameObject obj)
    {
        return Release(obj, fallbackReleaseDelegate);
    }

    static bool DefaultReleaseFallback(GameObject obj)
    {
        try
        {
            Object.Destroy(obj);
            return true;
        }
        catch
        {
            return false;
        }
    }
}